import React from "react";
import { ToastContainer, toast, Slide } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";
import styles from "./Notification.module.scss";

export interface NotificationAction {
  label: string;
  callback: () => void;
}

interface Props {
  message: string;
  action?: NotificationAction;
}

const NotificationComponent = ({
  message,
  action,
}: Props) => {
  return (
    <div className={styles.etNotificationWrapper}>
      <span className={styles.etNotificationInfo}>{message}</span>
      {action && (
        <button
          type="button"
          onClick={action.callback}
          className={styles.notificationButton}
        >
          {action.label}
        </button>
      )}
    </div>
  );
};

export const showNotification = (
  message: string,
  action?: NotificationAction,
) => {
  return toast(
    <NotificationComponent
      action={action}
      message={message}
    />,
    {
      className: styles.toastWrapper,
    }
  );
};

export const Notifications = () => {
  return (
    <ToastContainer
      draggable
      pauseOnHover
      hideProgressBar
      pauseOnFocusLoss
      autoClose={5000}
      transition={Slide}
      closeButton={false}
      newestOnTop={false}
      closeOnClick={false}
      draggablePercent={50}
      position="top-center"
      className={styles.etNotification}
    />
  );
};
