import React from 'react';
import ReactDOM from 'react-dom';
import { setLogger, QueryClientProvider, QueryClient } from "react-query";
import App from 'components/App';
import reportWebVitals from './reportWebVitals';

import 'assets/styles/core.scss';
import "./i18n";

if (process.env.NODE_ENV === "production") {
  setLogger({
    error: () => {},
    warn: () => {},
    log: () => {},
  });
}

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
      refetchOnWindowFocus: false,
    },
  },
})

ReactDOM.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
