import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import enTranslation from "assets/locales/en.json";
import uaTranslation from "assets/locales/ua.json";

const resources = {
  en: {
    translation: enTranslation,
  },
  ua: {
    translation: uaTranslation,
  },
};

i18n.use(initReactI18next).init({
  resources,
  lng: "ua",
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
