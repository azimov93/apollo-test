import { createContext } from "react";
import { GeoStatus, IGeolocationContext } from "../types";

const GeolocationContext = createContext<IGeolocationContext>({
  status: GeoStatus.INIT,
  position: null,
  error: null,
  selectedCity: null,
  setGeoPosition: () => {},
  setGeoPositionStatus: () => {},
  changeCity: () => {},
  setError: () => {},
});

export default GeolocationContext;
