import { createContext } from "react";
import { IUserContext } from "../types";

const UserContext = createContext<IUserContext>({
  isLoggedIn: false,
  data: null,
  setUserData: () => {},
});

export default UserContext;
