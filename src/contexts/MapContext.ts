import { createContext } from "react";
import { IMap, IMapContext, MapCenterTypes } from "../types";

export const initialMapData: IMap = {
  center: MapCenterTypes.User,
  position: {},
  bounds: [],
};

const MapContext = createContext<IMapContext>({
  data: initialMapData,
  setMapCenterType: () => {},
  setMapCoordinates: () => {},
  setBoundsPolygon: () => {},
});

export default MapContext;
