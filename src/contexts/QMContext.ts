import { createContext } from "react";
import { IQMContext } from "../types";

const QMContext = createContext<IQMContext>({
  isOpened: false,
  type: null,
  subtype: null,
  title: null,
  open: () => {},
  close: () => {}
});

export default QMContext;
