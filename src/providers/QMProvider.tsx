import React, { ReactNode, useState } from "react";
import { QMTypes, QMSubTypes, IQMProps } from "types";
import { QMContext } from "contexts";

interface IProps {
  children?: ReactNode;
}

const QMProvider = ({ children }: IProps) => {
  const [isOpened, setOpened] = useState(false);
  const [props, setProps] = useState<IQMProps | null>(null);

  const open = (type: QMTypes, subtype: QMSubTypes, title: string) => {
    setProps({
      type,
      subtype,
      title,
    });

    setOpened(true);
  };

  const close = () => {
    setOpened(false);
    setProps(null);
  };

  return (
    <QMContext.Provider
      value={{
        isOpened,
        props,
        open,
        close,
      }}
    >
      {children}
    </QMContext.Provider>
  );
};

export default QMProvider;
