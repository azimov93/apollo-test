import React, { ReactNode, useMemo, useState } from "react";
import {
  GeoStatus,
  ICity,
  IGeoError,
  IMapPosition,
  IUserPosition,
} from "types";
import { GeolocationContext } from "contexts";

interface IProps {
  children?: ReactNode;
}

const GeolocationProvider = ({ children }: IProps) => {
  const [status, setStatus] = useState<GeoStatus>(GeoStatus.INIT);
  const [errorCode, setErrorCode] = useState<number | null>(null);
  const [location, setLocation] = useState<IMapPosition>({});
  const [userCity, setUserCity] = useState<ICity | null>(null);
  const [selectedCity, setSelectedCity] = useState<ICity | null>(null);

  const position = useMemo<IUserPosition>(
    () => ({
      location,
      city: userCity,
    }),
    [location, userCity]
  );

  const error = useMemo<IGeoError | null>(() => {
    const isErrorStatus = status === GeoStatus.ERROR;

    if (isErrorStatus && errorCode) {
      return {
        code: errorCode,
      };
    }

    return null;
  }, [errorCode, status]);

  const setGeoPosition = (location: IMapPosition, city?: ICity) => {
    setLocation(location);

    if (city) {
      setUserCity(city);
    }
  };

  const setGeoPositionStatus = (status: GeoStatus) => setStatus(status);
  const changeCity = (city: ICity) => setSelectedCity(city);
  const setError = (errorCode: number | null) => setErrorCode(errorCode);

  return (
    <GeolocationContext.Provider
      value={{
        status,
        position,
        error,
        selectedCity,
        setGeoPosition,
        setGeoPositionStatus,
        changeCity,
        setError,
      }}
    >
      {children}
    </GeolocationContext.Provider>
  );
};

export default GeolocationProvider;
