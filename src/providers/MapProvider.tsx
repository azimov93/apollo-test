import React, { ReactNode, useMemo, useState } from "react";
import { IMapPosition, MapCenterTypes } from "types";
import { MapContext } from "contexts";

interface IProps {
  children?: ReactNode;
}

const MapProvider = ({ children }: IProps) => {
  const [centerType, setCenterType] = useState<MapCenterTypes>(MapCenterTypes.User);
  const [position, setPosition] = useState<IMapPosition>({});
  const [bounds, setBounds] = useState<number[][]>([]);

  const data = useMemo(() => ({
    center: centerType,
    position,
    bounds
  }), [centerType, position, bounds]);

  const setMapCenterType = (type: MapCenterTypes) => setCenterType(type);
  const setMapCoordinates = (position: IMapPosition) => setPosition(position);
  const setBoundsPolygon = (bounds: number[][]) => setBounds(bounds);

  return (
    <MapContext.Provider value={{
      data,
      setMapCenterType,
      setMapCoordinates,
      setBoundsPolygon,
    }}>
      {children}
    </MapContext.Provider>
  )
};

export default MapProvider;
