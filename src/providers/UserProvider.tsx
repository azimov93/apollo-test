import React, { ReactNode, useState } from "react";
import { IUser } from "types";
import { UserContext } from "contexts";

interface IProps {
  children?: ReactNode;
}

const UserProvider = ({ children }: IProps) => {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [user, setUser] = useState<IUser | null>(null);

  const setUserData = (data?: IUser) => {
    const update = !!data ? data : null
    setLoggedIn(!!update);
    setUser(update);
  }

  return (
    <UserContext.Provider value={{
      isLoggedIn,
      data: user,
      setUserData
    }}>
      {children}
    </UserContext.Provider>
  )
};

export default UserProvider;
