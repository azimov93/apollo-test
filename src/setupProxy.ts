const proxy = require('http-proxy-middleware');

const rewriteFn = function (path: string) {
	return path.replace('/api', '/api/v0');
};

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

module.exports = function (app: any) {
	app.use(proxy('/api/bar', {
		target: 'http://localhost:8001/',
		secure: false,
		pathRewrite: rewriteFn,
	}));
	app.use(proxy('/api/search', {
		target: 'http://localhost:8001/',
		secure: false,
		pathRewrite: rewriteFn,
	}));
	app.use(proxy('/api', {
		target: 'https://app.ecaffee.ml/',
		secure: false,
	}));
	// app.use(proxy('/ws', {
	// 	target: 'ws://localhost:7000/',
	// 	ws: true
	// }));
};
