import { IMapPosition } from "./map";

export enum GeoStatus {
  INIT,
  LOADED,
  UPDATING,
  ERROR,
}

export interface ICity {
  _id: string;
  code: string;
  country: string;
  bounds: number[];
  center: number[];
}

export interface IUserPosition {
  location: IMapPosition;
  city: ICity | null;
}

export interface IGeoError {
  code: number | null;
}

export interface IGeolocationContext {
  status: GeoStatus;
  position: IUserPosition | null;
  error: IGeoError | null;
  selectedCity: ICity | null;
  setGeoPosition: (location: IMapPosition, city?: ICity) => void;
  setGeoPositionStatus: (status: GeoStatus) => void;
  changeCity: (city: ICity) => void;
  setError: (errorCode: number | null) => void;
}
