export enum QMTypes {
  Auth = "auth",
  Bar = "bar",
}

export enum QMSubTypes {
  Welcome = "welcome",
  Login = "login",
  Register = "register",
  ConfirmPhone = "confirmPhone",
  Info = "info",
  Booking = "booking",
  Route = "route",
  Menu = "menu",
  Reviews = "reviews",
}

export interface IQMProps {
  type: QMTypes | null;
  subtype: QMSubTypes | null;
  title: string | null;
}

export interface IQMContext {
  isOpened: boolean;
  props: IQMProps | null,
  open: (type: QMTypes, subtype: QMSubTypes, title: string) => void;
  close: () => void;
}
