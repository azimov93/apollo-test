export interface IUser {
  username: string;
  email?: string;
  phone: string;
  fullName: string;
  activated: boolean;
}

export interface IUserContext {
  isLoggedIn: boolean;
  data: IUser | null;
  setUserData: (data?: IUser) => void;
}
