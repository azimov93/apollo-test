export enum MapCenterTypes {
  User = "user",
  Pin = "pin",
}

export interface IMapPosition {
  lng?: number;
  lat?: number;
  zoom?: number;
}

export interface IMap {
  center: MapCenterTypes;
  position?: IMapPosition;
  bounds: number[][];
}

export interface IMapContext {
  data: IMap;
  setMapCenterType: (type: MapCenterTypes) => void;
  setMapCoordinates: (position: IMapPosition) => void;
  setBoundsPolygon: (bounds: number[][]) => void;
}
