import React from "react";
import { BrowserRouter } from "react-router-dom";

import {
  UserProvider,
  MapProvider,
  GeolocationProvider,
  QMProvider,
} from "providers";
import { App, Notifications } from "components";

const AppContainer = () => {
  return (
    <>
      <UserProvider>
        <GeolocationProvider>
          <MapProvider>
            <QMProvider>
              <BrowserRouter>
                <App />
              </BrowserRouter>
              <Notifications />
            </QMProvider>
          </MapProvider>
        </GeolocationProvider>
      </UserProvider>
    </>
  );
};

export default AppContainer;
